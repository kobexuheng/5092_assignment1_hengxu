# README #

Monte Carlo Simulation for European Call/Put Options

Open MonteCarloSimulation.sln and run the project.
In the Monte Carlo Simulation form, choose call or put or call&put type of options.
Input S0,K,r,T,sigma and steps and trials. Press compute button.
There would be prompt if there is any wrong input.
The result would show call/put prices, greeks and standard errors.
