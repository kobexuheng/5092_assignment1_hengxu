﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MonteCarloSimulation
{
    public partial class Form_MCS : Form
    {
        public Form_MCS()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {   
            // clear text in result labels
            L_price.Text = "";
            L_delta.Text = "";
            L_gamma.Text = "";
            L_vega.Text = "";
            L_theta.Text = "";
            L_rho.Text = "";
            L_SE.Text = "";
        }

        private void B_compute_Click(object sender, EventArgs e)
        {
            //error handling
            //clear text in error handling label 
            L_wrong.Text = "";
            //1.lack option type
            if (R_call.Checked == false && R_put.Checked == false && R_callput.Checked == false)
            {
                L_wrong.Text =  "Lack option type";
            }
            //2.lack some inputs
            if (T_S0.Text == "" || T_K.Text == "" || T_r.Text == "" || T_T.Text == "" || T_sigma.Text == "" || T_step.Text == "" || T_trial.Text == "")
            {
                L_wrong.Text = L_wrong.Text + "\n" + "Lack input:";
                if (T_S0.Text == "") { L_wrong.Text = L_wrong.Text + " S0"; }
                if (T_K.Text == "") { L_wrong.Text = L_wrong.Text + " K"; }
                if (T_r.Text == "") { L_wrong.Text = L_wrong.Text + " r"; }
                if (T_T.Text == "") { L_wrong.Text = L_wrong.Text + " T"; }
                if (T_sigma.Text == "") { L_wrong.Text = L_wrong.Text + " sigma"; }
                if (T_step.Text == "") { L_wrong.Text = L_wrong.Text + " Steps"; }
                if (T_trial.Text == "") { L_wrong.Text = L_wrong.Text + " Trials"; }

            }
            //3.wrong inputs
            else
            {
                if (Convert.ToDouble(T_S0.Text) < 0) { L_wrong.Text = L_wrong.Text + "\n" + "wrong S0 input"; }
                if (Convert.ToDouble(T_K.Text) < 0) { L_wrong.Text = L_wrong.Text + "\n" + "wrong K input"; }
                if (Convert.ToDouble(T_r.Text) < 0) { L_wrong.Text = L_wrong.Text + "\n" + "wrong r input"; }
                if (Convert.ToDouble(T_T.Text) <= 0) { L_wrong.Text = L_wrong.Text + "\n" + "wrong T input"; }
                if (Convert.ToDouble(T_sigma.Text) <= 0) { L_wrong.Text = L_wrong.Text + "\n" + "wrong sigma input"; }
                if (Convert.ToDouble(T_step.Text) < 2) { L_wrong.Text = L_wrong.Text + "\n" + "wrong Steps input"; }
                if (Convert.ToDouble(T_trial.Text) < 1) { L_wrong.Text = L_wrong.Text + "\n" + "wrong Trials input"; }
            }
            
            //computation and output
            //decide if there is no input error
            if (L_wrong.Text == "")
            {
                //a instance of EuropeanOption class for computation
                EuropeanOption EO = new EuropeanOption();
                //get input from GUI
                double S0 = Convert.ToDouble(T_S0.Text);
                double K = Convert.ToDouble(T_K.Text);
                double r = Convert.ToDouble(T_r.Text);
                double T = Convert.ToDouble(T_T.Text);
                double sigma = Convert.ToDouble(T_sigma.Text);
                int trial = Convert.ToInt32(T_trial.Text);
                int step = Convert.ToInt32(T_step.Text);
                //out put for different type(call,put, call&put)
                if (R_call.Checked == true)
                {
                    String[] result = new string[7];
                    double[,] resultdata = EO.EuroData(S0, K, r, T, sigma, trial, step);
                    for(int i = 1; i<=7; i++ )
                    { result[i - 1] = Convert.ToString(resultdata[0,i - 1]); }
                    L_price.Text = "call price = " + result[0];
                    L_delta.Text = "delta = " + result[1];
                    L_gamma.Text = "gamma = " + result[2];
                    L_vega.Text = "vega = " + result[3];
                    L_theta.Text = "theat = " + result[4];
                    L_rho.Text = "rho = " + result[5];
                    L_SE.Text = "standard error = " + result[6];
                }
                else if (R_put.Checked == true)
                {
                    String[] result = new string[7];
                    double[,] resultdata = EO.EuroData(S0, K, r, T, sigma, trial, step);
                    for (int i = 1; i <= 7; i++)
                    { result[i - 1] = Convert.ToString(resultdata[1, i - 1]); }
                    L_price.Text = "put price = " + result[0];
                    L_delta.Text = "delta = " + result[1];
                    L_gamma.Text = "gamma = " + result[2];
                    L_vega.Text = "vega = " + result[3];
                    L_theta.Text = "theat = " + result[4];
                    L_rho.Text = "rho = " + result[5];
                    L_SE.Text = "standard error = " + result[6];
                }
                else if (R_callput.Checked == true)
                {
                    String[,] result = new string[2,7];
                    double[,] resultdata = EO.EuroData(S0, K, r, T, sigma, trial, step);
                    for (int i = 1; i <= 7; i++)
                    { result[0,i - 1] = Convert.ToString(resultdata[0, i - 1]); }
                    for(int i = 1; i <= 7; i++)
                    { result[1,i - 1] = Convert.ToString(resultdata[1, i - 1]); }

                    L_price.Text = "call price = " + result[0,0]+ ", put price = "+ result[1,0];
                    L_delta.Text = "call delta = " + result[0,1] + ", put delta = " + result[1, 1];
                    L_gamma.Text = "call gamma = " + result[0,2] + ", put gamma = " + result[1,2 ];
                    L_vega.Text = "call vega = " + result[0,3]+ ", put vega = "+ result[1,3];
                    L_theta.Text = "call theat = " + result[0,4]+ ", put theta = " + result[1,4];
                    L_rho.Text = "call rho = " + result[0,5]+ ", put rho = "+ result[1,5];
                    L_SE.Text = "call standard error = " + result[0,6]+ ", put standard error = "+ result[1,6];
                }
            }
            

        }

        
    }
}
