﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonteCarloSimulation
{
    class Simulator
    {

        //Method for generating random numbers(Box-Muller)
        public double[,] BoxMullerRandom(int trial, int number)
        {
            double[,] randomnumber = new double[trial, number];
            double a1, a2, rv1, rv2;
            int i, j;
            Random rnd = new Random();
            //generate random values(the number is trial*(step-1))
            //number is even
            if (number % 2 == 0)
            {
                for (i = 1; i <= trial; i++)
                {
                    for (j = 1; j <= (number / 2); j++)
                    {
                        a1 = rnd.NextDouble();
                        a2 = rnd.NextDouble();
                        rv1 = Math.Sqrt(-2 * Math.Log(a1)) * Math.Cos(2 * Math.PI * a2);
                        rv2 = Math.Sqrt(-2 * Math.Log(a1)) * Math.Sin(2 * Math.PI * a2);
                        randomnumber[i - 1, j * 2 - 2] = rv1;
                        randomnumber[i - 1, j * 2 - 1] = rv2;
                    }
                }
            }
            //number is odd
            else
            {
                for (i = 1; i <= trial; i++)
                {

                    for (j = 1; j <= ((number - 1) / 2); j++)
                    {
                        a1 = rnd.NextDouble();
                        a2 = rnd.NextDouble();
                        rv1 = Math.Sqrt(-2 * Math.Log(a1)) * Math.Cos(2 * Math.PI * a2);
                        rv2 = Math.Sqrt(-2 * Math.Log(a1)) * Math.Sin(2 * Math.PI * a2);
                        randomnumber[i - 1, j * 2 - 2] = rv1;
                        randomnumber[i - 1, j * 2 - 1] = rv2;
                    }
                    a1 = rnd.NextDouble();
                    a2 = rnd.NextDouble();
                    rv1 = Math.Sqrt(-2 * Math.Log(a1)) * Math.Cos(2 * Math.PI * a2);
                    rv2 = Math.Sqrt(-2 * Math.Log(a1)) * Math.Sin(2 * Math.PI * a2);
                    randomnumber[i - 1, number - 1] = rv1;

                }

            }
            return randomnumber;

        }
        

        //Method for underlying prices(with only final prices)
        public double[] Underlyingfinal(double S0, double r, double sigma, double T, int step, int trial)
        {
            double[] price = new double[trial];
            double[,] random = BoxMullerRandom(trial,step-1);
            int i, j;
            for (i = 1; i <= trial; i++)
            {
                double sum = 0;
                for(j=1;j<=step-1;j++)
                { sum = sum + random[i - 1, j - 1]; }
                // final underlying prices for every trial
                price[i - 1] = S0 * Math.Exp((r - 0.5 * sigma * sigma) * T + sigma * Math.Sqrt(T/(step-1)) * sum);
                  
            }

            return price;
        }

        //Method for underlying prices(with all nodes)
        public double[,] Underlying( double S0 , double r, double sigma , double T, int step , int trial)
        {
            double[,] price = new double[trial, step];
            double[,] random = BoxMullerRandom(trial,step-1);
            int i,j;
            double S;
            double t = T / (step - 1) ;
            // underlying prices simulation( i for tirals , j for steps)  
            for(i=1 ; i<=trial ; i++)
            {
                S = S0;
                price[i - 1, 0] = S0;
                for (j = 1; j <= step-1; j++)
                {   
                    S = S * Math.Exp((r - 0.5 * sigma * sigma) * t + sigma * Math.Sqrt(t) * random[i-1,j-1]);
                    price[i - 1, j ] = S;

                }

            }

            return price;
        }
        

       






    }
}
