﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonteCarloSimulation
{
    class EuropeanOption:Option
    {
        // declare a simulator class for random numbers
        Simulator SimO = new Simulator();
        // method for all the output
        public double[,] EuroData(double S0, double K, double r, double T, double sigma, int trial, int step)
        {

            double[,] resultmatrix = new double[2,7];
            //get matrix of normal random values from a simulator class
            double[,] randommatrix = SimO.BoxMullerRandom(trial, step);
            //assign values to array be returned (using call and put methods below);
            //call
            resultmatrix[0, 0] = Callprice(S0, K, r, T, sigma, trial, step)[0];
            resultmatrix[0, 1] = (Callprice(1.001 * S0, K, r, T, sigma, trial, step)[0] - Callprice(0.999 * S0, K, r, T, sigma, trial, step)[0]) / (0.002 * S0);
            resultmatrix[0, 2] = (Callprice(1.001 * S0, K, r, T, sigma, trial, step)[0] - 2*Callprice(S0,K,r,T,sigma,trial,step)[0] + Callprice(0.999*S0, K, r, T, sigma, trial, step)[0]) / Math.Pow((0.001 * S0),2);
            resultmatrix[0, 3] = (Callprice( S0, K, r, T, 1.001 * sigma, trial, step)[0] - Callprice( S0, K, r, T, 0.999 * sigma, trial, step)[0]) / (0.002 * sigma);
            resultmatrix[0, 4] = (Callprice(S0, K, r, 1.001 * T, sigma, trial, step)[0] - Callprice( S0, K, r, T, sigma, trial, step)[0]) / (0.001 * T);
            resultmatrix[0, 5] = (Callprice( S0, K, 1.001 * r, T, sigma, trial, step)[0] - Callprice( S0, K, 0.999 * r, T, sigma, trial, step)[0]) / (0.002 * r);
            resultmatrix[0, 6] = Callprice(S0, K, 1.001 * r, T, sigma, trial, step)[1];
            //put
            resultmatrix[1, 0] = Putprice(S0, K, r, T, sigma, trial, step)[0];
            resultmatrix[1, 1] = (Putprice(1.001 * S0, K, r, T, sigma, trial, step)[0] - Putprice(0.999 * S0, K, r, T, sigma, trial, step)[0]) / (0.002 * S0);
            resultmatrix[1, 2] = (Putprice(1.001 * S0, K, r, T, sigma, trial, step)[0] - 2 * Putprice(S0, K, r, T, sigma, trial, step)[0] + Putprice(0.999 * S0, K, r, T, sigma, trial, step)[0]) / Math.Pow((0.001 * S0), 2);
            resultmatrix[1, 3] = (Putprice(S0, K, r, T, 1.001 * sigma, trial, step)[0] - Putprice(S0, K, r, T, 0.999 * sigma, trial, step)[0]) / (0.002 * sigma);
            resultmatrix[1, 4] = (Putprice(S0, K, r, 1.001 * T, sigma, trial, step)[0] - Putprice(S0, K, r, T, sigma, trial, step)[0]) / (0.001 * T);
            resultmatrix[1, 5] = (Putprice(S0, K, 1.001 * r, T, sigma, trial, step)[0] - Putprice(S0, K, 0.999 * r, T, sigma, trial, step)[0]) / (0.002 * r);
            resultmatrix[1, 6] = Putprice(S0, K, 1.001 * r, T, sigma, trial, step)[1];
            

            //inside method for call prices and standard error
            double [] Callprice(double S01, double K1, double r1, double T1, double sigma1, int trial1, int step1)
            {
                //underlying prices at T
                double[] underlying_final_price = new double[trial1];
                int i, j;
                for (i = 1; i <= trial1; i++)
                {
                    double sum = 0;
                    for (j = 1; j <= step1 - 1; j++)
                    { sum = sum + randommatrix[i - 1, j - 1]; }
                    underlying_final_price[i - 1] = S01 * Math.Exp((r1 - 0.5 * sigma1 * sigma1) * T1 + sigma1 * Math.Sqrt(T1 / (step1 - 1)) * sum);
                }
                //option prices at T
                double [] option_final_price = new double[trial1];
                for (int count=1;count<=trial1;count++)
                {
                    option_final_price[count-1] = Math.Max(underlying_final_price[count - 1] - K1, 0);
                }
                //output V0 and SE
                 double[] V_SE = new double[2];
                 V_SE[0] = (option_final_price.Average()) * Math.Exp(-r1 * T1);
                 double sumSE = 0;
                 for(int count=1; count<=trial1 ;count++)
                {
                    sumSE = sumSE + Math.Pow((option_final_price[count - 1] * Math.Exp(-r1 * T1) - V_SE[0]), 2);
                }
                V_SE[1] = Math.Sqrt((sumSE / (trial1-1))/(trial1 )); 
                return V_SE;
            }
            //inside method for call prices and standard error
            double[] Putprice(double S01, double K1, double r1, double T1, double sigma1, int trial1, int step1)
            {
                //underlying prices at T
                double[] underlying_final_price = new double[trial1];
                int i, j;
                for (i = 1; i <= trial1; i++)
                {
                    double sum = 0;
                    for (j = 1; j <= step1 - 1; j++)
                    { sum = sum + randommatrix[i - 1, j - 1]; }
                    underlying_final_price[i - 1] = S01 * Math.Exp((r1 - 0.5 * sigma1 * sigma1) * T1 + sigma1 * Math.Sqrt(T1 / (step1 - 1)) * sum);
                }
                //option prices at T
                double[] option_final_price = new double[trial1];
                for (int count = 1; count <= trial1; count++)
                {
                    option_final_price[count - 1] = Math.Max(K1 - underlying_final_price[count - 1], 0);
                }
                //output V0 and SE
                double[] V_SE = new double[2];
                V_SE[0] = (option_final_price.Average()) * Math.Exp(-r1 * T1);
                double sumSE = 0;
                for (int count = 1; count <= trial1; count++)
                {
                    sumSE = sumSE + Math.Pow((option_final_price[count - 1] * Math.Exp(-r1 * T1) - V_SE[0]), 2);
                }
                V_SE[1] = Math.Sqrt((sumSE / (trial1-1 )) / (trial1 ));
                return V_SE;

            }
            
            return resultmatrix;
        }
        
        
        
    }  
}
