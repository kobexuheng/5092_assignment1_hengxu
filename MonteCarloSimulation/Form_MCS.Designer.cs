﻿namespace MonteCarloSimulation
{
    partial class Form_MCS
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.L_input = new System.Windows.Forms.Label();
            this.L_S0 = new System.Windows.Forms.Label();
            this.L_K = new System.Windows.Forms.Label();
            this.L_r = new System.Windows.Forms.Label();
            this.L_T = new System.Windows.Forms.Label();
            this.T_S0 = new System.Windows.Forms.TextBox();
            this.T_K = new System.Windows.Forms.TextBox();
            this.T_r = new System.Windows.Forms.TextBox();
            this.T_T = new System.Windows.Forms.TextBox();
            this.L_sigma = new System.Windows.Forms.Label();
            this.T_sigma = new System.Windows.Forms.TextBox();
            this.L_simulation = new System.Windows.Forms.Label();
            this.B_compute = new System.Windows.Forms.Button();
            this.L_Eurooption = new System.Windows.Forms.Label();
            this.R_call = new System.Windows.Forms.RadioButton();
            this.R_put = new System.Windows.Forms.RadioButton();
            this.L_step = new System.Windows.Forms.Label();
            this.L_trial = new System.Windows.Forms.Label();
            this.L_result = new System.Windows.Forms.Label();
            this.L_price = new System.Windows.Forms.Label();
            this.L_delta = new System.Windows.Forms.Label();
            this.L_gamma = new System.Windows.Forms.Label();
            this.L_vega = new System.Windows.Forms.Label();
            this.L_theta = new System.Windows.Forms.Label();
            this.L_rho = new System.Windows.Forms.Label();
            this.L_SE = new System.Windows.Forms.Label();
            this.L_wrong = new System.Windows.Forms.Label();
            this.T_step = new System.Windows.Forms.TextBox();
            this.T_trial = new System.Windows.Forms.TextBox();
            this.R_callput = new System.Windows.Forms.RadioButton();
            this.SuspendLayout();
            // 
            // L_input
            // 
            this.L_input.AutoSize = true;
            this.L_input.Location = new System.Drawing.Point(22, 53);
            this.L_input.Name = "L_input";
            this.L_input.Size = new System.Drawing.Size(41, 12);
            this.L_input.TabIndex = 0;
            this.L_input.Text = "Input:";
            // 
            // L_S0
            // 
            this.L_S0.AutoSize = true;
            this.L_S0.Location = new System.Drawing.Point(22, 83);
            this.L_S0.Name = "L_S0";
            this.L_S0.Size = new System.Drawing.Size(17, 12);
            this.L_S0.TabIndex = 1;
            this.L_S0.Text = "S0";
            // 
            // L_K
            // 
            this.L_K.AutoSize = true;
            this.L_K.Location = new System.Drawing.Point(22, 113);
            this.L_K.Name = "L_K";
            this.L_K.Size = new System.Drawing.Size(11, 12);
            this.L_K.TabIndex = 2;
            this.L_K.Text = "K";
            // 
            // L_r
            // 
            this.L_r.AutoSize = true;
            this.L_r.Location = new System.Drawing.Point(22, 143);
            this.L_r.Name = "L_r";
            this.L_r.Size = new System.Drawing.Size(11, 12);
            this.L_r.TabIndex = 4;
            this.L_r.Text = "r";
            // 
            // L_T
            // 
            this.L_T.AutoSize = true;
            this.L_T.Location = new System.Drawing.Point(22, 173);
            this.L_T.Name = "L_T";
            this.L_T.Size = new System.Drawing.Size(11, 12);
            this.L_T.TabIndex = 5;
            this.L_T.Text = "T";
            // 
            // T_S0
            // 
            this.T_S0.Location = new System.Drawing.Point(88, 80);
            this.T_S0.Name = "T_S0";
            this.T_S0.Size = new System.Drawing.Size(66, 21);
            this.T_S0.TabIndex = 6;
            // 
            // T_K
            // 
            this.T_K.Location = new System.Drawing.Point(88, 110);
            this.T_K.Name = "T_K";
            this.T_K.Size = new System.Drawing.Size(66, 21);
            this.T_K.TabIndex = 7;
            // 
            // T_r
            // 
            this.T_r.Location = new System.Drawing.Point(88, 140);
            this.T_r.Name = "T_r";
            this.T_r.Size = new System.Drawing.Size(66, 21);
            this.T_r.TabIndex = 8;
            // 
            // T_T
            // 
            this.T_T.Location = new System.Drawing.Point(88, 170);
            this.T_T.Name = "T_T";
            this.T_T.Size = new System.Drawing.Size(66, 21);
            this.T_T.TabIndex = 9;
            // 
            // L_sigma
            // 
            this.L_sigma.AutoSize = true;
            this.L_sigma.Location = new System.Drawing.Point(22, 203);
            this.L_sigma.Name = "L_sigma";
            this.L_sigma.Size = new System.Drawing.Size(35, 12);
            this.L_sigma.TabIndex = 10;
            this.L_sigma.Text = "sigma";
            // 
            // T_sigma
            // 
            this.T_sigma.Location = new System.Drawing.Point(89, 200);
            this.T_sigma.Name = "T_sigma";
            this.T_sigma.Size = new System.Drawing.Size(65, 21);
            this.T_sigma.TabIndex = 11;
            // 
            // L_simulation
            // 
            this.L_simulation.AutoSize = true;
            this.L_simulation.Location = new System.Drawing.Point(22, 241);
            this.L_simulation.Name = "L_simulation";
            this.L_simulation.Size = new System.Drawing.Size(71, 12);
            this.L_simulation.TabIndex = 12;
            this.L_simulation.Text = "Simulation:";
            // 
            // B_compute
            // 
            this.B_compute.Location = new System.Drawing.Point(187, 270);
            this.B_compute.Name = "B_compute";
            this.B_compute.Size = new System.Drawing.Size(119, 40);
            this.B_compute.TabIndex = 13;
            this.B_compute.Text = "compute";
            this.B_compute.UseVisualStyleBackColor = true;
            this.B_compute.Click += new System.EventHandler(this.B_compute_Click);
            // 
            // L_Eurooption
            // 
            this.L_Eurooption.AutoSize = true;
            this.L_Eurooption.Location = new System.Drawing.Point(22, 22);
            this.L_Eurooption.Name = "L_Eurooption";
            this.L_Eurooption.Size = new System.Drawing.Size(95, 12);
            this.L_Eurooption.TabIndex = 14;
            this.L_Eurooption.Text = "European Option";
            // 
            // R_call
            // 
            this.R_call.AutoSize = true;
            this.R_call.Location = new System.Drawing.Point(134, 20);
            this.R_call.Name = "R_call";
            this.R_call.Size = new System.Drawing.Size(47, 16);
            this.R_call.TabIndex = 15;
            this.R_call.TabStop = true;
            this.R_call.Text = "Call";
            this.R_call.UseVisualStyleBackColor = true;
            // 
            // R_put
            // 
            this.R_put.AutoSize = true;
            this.R_put.Location = new System.Drawing.Point(187, 20);
            this.R_put.Name = "R_put";
            this.R_put.Size = new System.Drawing.Size(41, 16);
            this.R_put.TabIndex = 16;
            this.R_put.TabStop = true;
            this.R_put.Text = "Put";
            this.R_put.UseVisualStyleBackColor = true;
            // 
            // L_step
            // 
            this.L_step.AutoSize = true;
            this.L_step.Location = new System.Drawing.Point(22, 270);
            this.L_step.Name = "L_step";
            this.L_step.Size = new System.Drawing.Size(35, 12);
            this.L_step.TabIndex = 17;
            this.L_step.Text = "Steps";
            // 
            // L_trial
            // 
            this.L_trial.AutoSize = true;
            this.L_trial.Location = new System.Drawing.Point(22, 298);
            this.L_trial.Name = "L_trial";
            this.L_trial.Size = new System.Drawing.Size(41, 12);
            this.L_trial.TabIndex = 18;
            this.L_trial.Text = "Trials";
            // 
            // L_result
            // 
            this.L_result.AutoSize = true;
            this.L_result.Location = new System.Drawing.Point(185, 53);
            this.L_result.Name = "L_result";
            this.L_result.Size = new System.Drawing.Size(113, 12);
            this.L_result.TabIndex = 19;
            this.L_result.Text = "Result shows here:";
            // 
            // L_price
            // 
            this.L_price.AutoSize = true;
            this.L_price.Location = new System.Drawing.Point(211, 80);
            this.L_price.Name = "L_price";
            this.L_price.Size = new System.Drawing.Size(35, 12);
            this.L_price.TabIndex = 20;
            this.L_price.Text = "price";
            // 
            // L_delta
            // 
            this.L_delta.AutoSize = true;
            this.L_delta.Location = new System.Drawing.Point(211, 105);
            this.L_delta.Name = "L_delta";
            this.L_delta.Size = new System.Drawing.Size(35, 12);
            this.L_delta.TabIndex = 21;
            this.L_delta.Text = "delta";
            // 
            // L_gamma
            // 
            this.L_gamma.AutoSize = true;
            this.L_gamma.Location = new System.Drawing.Point(211, 129);
            this.L_gamma.Name = "L_gamma";
            this.L_gamma.Size = new System.Drawing.Size(35, 12);
            this.L_gamma.TabIndex = 22;
            this.L_gamma.Text = "gamma";
            // 
            // L_vega
            // 
            this.L_vega.AutoSize = true;
            this.L_vega.Location = new System.Drawing.Point(211, 151);
            this.L_vega.Name = "L_vega";
            this.L_vega.Size = new System.Drawing.Size(29, 12);
            this.L_vega.TabIndex = 23;
            this.L_vega.Text = "vega";
            // 
            // L_theta
            // 
            this.L_theta.AutoSize = true;
            this.L_theta.Location = new System.Drawing.Point(211, 179);
            this.L_theta.Name = "L_theta";
            this.L_theta.Size = new System.Drawing.Size(35, 12);
            this.L_theta.TabIndex = 24;
            this.L_theta.Text = "theta";
            // 
            // L_rho
            // 
            this.L_rho.AutoSize = true;
            this.L_rho.Location = new System.Drawing.Point(211, 200);
            this.L_rho.Name = "L_rho";
            this.L_rho.Size = new System.Drawing.Size(23, 12);
            this.L_rho.TabIndex = 25;
            this.L_rho.Text = "rho";
            // 
            // L_SE
            // 
            this.L_SE.AutoSize = true;
            this.L_SE.Location = new System.Drawing.Point(211, 231);
            this.L_SE.Name = "L_SE";
            this.L_SE.Size = new System.Drawing.Size(89, 12);
            this.L_SE.TabIndex = 26;
            this.L_SE.Text = "standard error";
            // 
            // L_wrong
            // 
            this.L_wrong.AutoSize = true;
            this.L_wrong.ForeColor = System.Drawing.Color.Red;
            this.L_wrong.Location = new System.Drawing.Point(335, 53);
            this.L_wrong.Name = "L_wrong";
            this.L_wrong.Size = new System.Drawing.Size(95, 12);
            this.L_wrong.TabIndex = 27;
            this.L_wrong.Text = "Error handling:";
            // 
            // T_step
            // 
            this.T_step.Location = new System.Drawing.Point(89, 261);
            this.T_step.Name = "T_step";
            this.T_step.Size = new System.Drawing.Size(65, 21);
            this.T_step.TabIndex = 28;
            // 
            // T_trial
            // 
            this.T_trial.Location = new System.Drawing.Point(88, 295);
            this.T_trial.Name = "T_trial";
            this.T_trial.Size = new System.Drawing.Size(66, 21);
            this.T_trial.TabIndex = 29;
            // 
            // R_callput
            // 
            this.R_callput.AutoSize = true;
            this.R_callput.Location = new System.Drawing.Point(234, 20);
            this.R_callput.Name = "R_callput";
            this.R_callput.Size = new System.Drawing.Size(95, 16);
            this.R_callput.TabIndex = 31;
            this.R_callput.TabStop = true;
            this.R_callput.Text = "Call and Put";
            this.R_callput.UseVisualStyleBackColor = true;
            // 
            // Form_MCS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(694, 340);
            this.Controls.Add(this.R_callput);
            this.Controls.Add(this.T_trial);
            this.Controls.Add(this.T_step);
            this.Controls.Add(this.L_wrong);
            this.Controls.Add(this.L_SE);
            this.Controls.Add(this.L_rho);
            this.Controls.Add(this.L_theta);
            this.Controls.Add(this.L_vega);
            this.Controls.Add(this.L_gamma);
            this.Controls.Add(this.L_delta);
            this.Controls.Add(this.L_price);
            this.Controls.Add(this.L_result);
            this.Controls.Add(this.L_trial);
            this.Controls.Add(this.L_step);
            this.Controls.Add(this.R_put);
            this.Controls.Add(this.R_call);
            this.Controls.Add(this.L_Eurooption);
            this.Controls.Add(this.B_compute);
            this.Controls.Add(this.L_simulation);
            this.Controls.Add(this.T_sigma);
            this.Controls.Add(this.L_sigma);
            this.Controls.Add(this.T_T);
            this.Controls.Add(this.T_r);
            this.Controls.Add(this.T_K);
            this.Controls.Add(this.T_S0);
            this.Controls.Add(this.L_T);
            this.Controls.Add(this.L_r);
            this.Controls.Add(this.L_K);
            this.Controls.Add(this.L_S0);
            this.Controls.Add(this.L_input);
            this.Name = "Form_MCS";
            this.Text = "Monte Carlo Simulation";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label L_input;
        private System.Windows.Forms.Label L_S0;
        private System.Windows.Forms.Label L_K;
        private System.Windows.Forms.Label L_r;
        private System.Windows.Forms.Label L_T;
        private System.Windows.Forms.TextBox T_S0;
        private System.Windows.Forms.TextBox T_K;
        private System.Windows.Forms.TextBox T_r;
        private System.Windows.Forms.TextBox T_T;
        private System.Windows.Forms.Label L_sigma;
        private System.Windows.Forms.TextBox T_sigma;
        private System.Windows.Forms.Label L_simulation;
        private System.Windows.Forms.Button B_compute;
        private System.Windows.Forms.Label L_Eurooption;
        private System.Windows.Forms.RadioButton R_call;
        private System.Windows.Forms.RadioButton R_put;
        private System.Windows.Forms.Label L_step;
        private System.Windows.Forms.Label L_trial;
        private System.Windows.Forms.Label L_result;
        private System.Windows.Forms.Label L_price;
        private System.Windows.Forms.Label L_delta;
        private System.Windows.Forms.Label L_gamma;
        private System.Windows.Forms.Label L_vega;
        private System.Windows.Forms.Label L_theta;
        private System.Windows.Forms.Label L_rho;
        private System.Windows.Forms.Label L_SE;
        private System.Windows.Forms.Label L_wrong;
        private System.Windows.Forms.TextBox T_step;
        private System.Windows.Forms.TextBox T_trial;
        private System.Windows.Forms.RadioButton R_callput;
    }
}

